import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color;

/**
 * Keeps the score
 * 
 * @author Huu Vu
 * @version 1.0
 */
public class Count extends Actor
{
    static final Color tColor = new Color(255, 51, 51); // Text's Color
    static int // Team Score
    team1 = 0,
    team2 = 0,
    team3 = 0,
    team4 = 0,
    teamUp = 1; // Team that is answering

    String text = "";

    public Count(int player) {
        teamUp = player;
    }
    
    /*
     * Add Score
     */
    static public void check(int score){
        if (teamUp == 1) team1 += score;
        else if (teamUp == 2) team2 += score;
        else if (teamUp == 3) team3 += score;
        else if (teamUp == 4) team4 += score;

        if (teamUp != 4) teamUp++; // Go to the next team
        else if (teamUp == 4) teamUp = 1; // Go back to the first team
    }

    /**
     * Change the Value of Text
     */
    public void act() 
    {
        text = "Team 1: " + team1 + "    Team 2: " + team2 + "    Team 3: " + team3 + "    Team 4: " + team4;
        if (teamUp == 1) text += "\n\t\t\tTeam 1 Turn!";
        else if (teamUp == 2) text += "\n\t\t\tTeam 2 Turn!";
        else if (teamUp == 3) text += "\n\t\t\tTeam 3 Turn!";
        else if (teamUp == 4) text += "\n\t\t\tTeam 4 Turn!";

        UpdateImage();
    }

    /*
     * Return the score of the current player
     */
    static public int score(int teamUp) {
        if (teamUp == 1) return team1;
        else if (teamUp == 2) return team2;
        else if (teamUp == 3) return team3;
        else if (teamUp == 4) return team4;
        return 0;
    }

    /*
     * Draw the text on screen
     */
    public void UpdateImage() {
        setImage(new GreenfootImage((text.length() + 2) * 10, 16));
        GreenfootImage tImage = new GreenfootImage(text, 30 , Color.RED, new Color(0,0,0,0));
        setImage(tImage	);
    }
}

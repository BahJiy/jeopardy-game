import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import java.awt.Font;
import java.awt.Color;

/**
 * Text of all Questions and Answer
 * 
 * @author Huu Vu
 * @version 1.0
 */
public class Text extends Actor
{
    static int nQuestion = 0, // Number of Question Answer (used for Final Jeopary)
    dJchance = 100; // Chances of geting Double Jeopardy 
    int
    type, // Type of Image
    col, // Column question is in
    x, // X
    y; // Y

    Object[] a = {"Valid!", "Invalid."}; // Answer Choices
    public Text(int type) {
        if (type == -6) {
            String text = "Answer: 2 Daughters, 0 Sons";

            setImage(new GreenfootImage((text.length() + 2) * 10, 16));
            GreenfootImage image = new GreenfootImage(text, 50, Color.RED, new Color(0, 0, 0, 0));
            setImage(image); //Set the Image
        } else if (type == -5) {
            String text = "How many daughters and sons\ndo Barack Obama have?";

            setImage(new GreenfootImage((text.length() + 2) * 10, 16));
            GreenfootImage image = new GreenfootImage(text, 50, Color.RED, new Color(0, 0, 0, 0));
            setImage(image); //Set the Image
        } else if (type == -1) {setImage("t1.png");}
        else if (type == 0) {setImage("t2.png");}
        else if (type == 1) {setImage("cat.png");}
        else if (type == 2) {setImage("final.png");}
    }

    public Text(int type, int col, int x, int y){
        if (type == 100) {setImage("1.png");}
        else if (type == 200) {setImage("2.png");}
        else if (type == 300) {setImage("3.png");}
        else if (type == 400) {setImage("4.png");}
        else if (type == 500) {setImage("5.png");}        
        this.x = x;
        this.y = y;
        this.type = type;
        this.col = col;
        javax.swing.UIManager.put("OptionPane.messageFont", new FontUIResource(new Font("Times New Roman", Font.PLAIN, 17))); 
    }

    public Text (String text) {
        setImage(new GreenfootImage((text.length() + 2) * 10, 16));
        GreenfootImage image = new GreenfootImage(text, 50, Color.RED, new Color(0, 0, 0, 0));
        setImage(image); //Set the Image
    }

    /**
     * Check Clicks and Display Message
     */
    public void act() 
    {
        // If player click this.Object and this.Object is a question
        if (Greenfoot.mouseClicked(this) && type > 5){
            int 
            CW = -1, // 0 = Correct 1 = Wrong
            dJ = Greenfoot.getRandomNumber(dJchance),
            point = 0, // Point bet
            maxPoint; // Maxium Point Value
            dJchance -= 10; // Make it easier to get DJ

            /**
             * Double Jeopary
             */
            if (dJ <= 3) {
                dJchance = 100; // Reset Double Jeopary chances                
                if (Count.score(Count.teamUp) >= 0) // Max Point can bet
                    maxPoint = Count.score(Count.teamUp) + 1000;
                else   maxPoint = 1000;
                Greenfoot.playSound("double.mp3"); // Play Double Jeopary Music
                while (point < 1){
                    String s = JOptionPane.showInputDialog(null, " Input your bet.\n(Have to be between: 1 - " + maxPoint); // Ask for bet amount.
                    try {
                        if (s != null || s.length() >= 1) point = Integer.parseInt(s); // Parse the string into an integer
                    } catch(NumberFormatException e){
                        point = 0;
                    }
                    if (point > maxPoint || point <= 0) {
                        JOptionPane .showMessageDialog  (null, "Invalid Respond!"); // If the answer is 0 or above score + 1000
                        point = 0;
                    }
                }
            }
            // Definition
            if (type == 100 && col == 1) {
                JOptionPane.showMessageDialog(null, "Legislative Branch", "Definition: 100",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Congress", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 200 && col == 1) {
                JOptionPane.showMessageDialog(null, "Person that execute the law faithfully", "Definition: 200",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "President","Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 300 && col == 1) {
                JOptionPane.showMessageDialog(null, "This person is the current Chief Justice", "Definition: 300",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "John Glover Roberts", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 400 && col == 1) {
                JOptionPane.showMessageDialog(null, "Provides for the election of the President and Vice President", "Definition: 400",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "12th Amendment", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 500 && col == 1) {
                JOptionPane.showMessageDialog(null, "This prevent states from discriminating out-of-state citizen", "Definition: 500",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Privileges and Immunities Clause", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}

            // Define
            if (type == 100 && col == 2) {
                JOptionPane.showMessageDialog(null, "5th Amendment (List 3+ parts)", "Define: 100",  JOptionPane.QUESTION_MESSAGE);
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Provides rights for criminal and civil proceedings; Guarantees right to grand jury;\nForbid double jeopardy; Protects against self-incrimination; Due process", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 200 && col == 2) {
                JOptionPane.showMessageDialog(null, "Iron Triangle", "Define: 200",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "The connection between Congress, Bureaucracies, and Special Interest Groups", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 300 && col == 2) {
                JOptionPane.showMessageDialog(null, "Gideon v. Wainwright", "Define: 300",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Part of the Selective Incorparation\nForce states to provide counsel in a criminal case", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 400 && col == 2) {
                JOptionPane.showMessageDialog(null, "Virginia Plan", "Define: 400",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Provides for a bicameral legislative branch with each house being represented based on population", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 500 && col == 2) {
                JOptionPane.showMessageDialog(null, "Select Committees", "Define: 500",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Created to investigate a scandal or special problem.\nExists only for a limited time.", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}

            //Define and Defination
            if (type == 100 && col == 3) {
                JOptionPane.showMessageDialog(null, "Executive Order", "Define or Definition: 100",  JOptionPane.QUESTION_MESSAGE);
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Command from the President that carries the force of the law.", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 200 && col == 3) {
                JOptionPane.showMessageDialog(null, "Where does a bill goes when both house of Congress pass different version?", "Define or Definition: 200",  JOptionPane.QUESTION_MESSAGE);
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Conference Committees", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 300 && col == 3) {
                JOptionPane.showMessageDialog(null, "States that it is unconstitutional to limit an individual's spending to support a\ncandidate outside of the campaign", "Define or Definition: 300",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Buckley v. Valeo", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 400 && col == 3) {
                JOptionPane.showMessageDialog(null, "The Campaign Finance Reform Act set the limit for hard money to what?", "Define or Definition: 400",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "$2,000", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 500 && col == 3) {
                JOptionPane.showMessageDialog(null, "Audits money spent by agencies and investigates agencies & polices (Provide the full name)", "Define or Definition: 500",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "General Accountability Office", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}

            // Questions
            if (type == 100 && col == 4) {
                JOptionPane.showMessageDialog(null, "What is the OMB (State full name and definition)", "Questions: 100",  JOptionPane.QUESTION_MESSAGE);
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Office of Management and Budget. Prepare the budget for the current fiscal year", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 200 && col == 4) {
                JOptionPane.showMessageDialog(null, "What agency in Congress check over the budget and make any adjustments? (Provide the full name)", "Questions: 200",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Congressional Budget Office", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 300 && col == 4) {
                JOptionPane.showMessageDialog(null, "What did the case Clinton v. New York do?", "Questions: 300",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Ban Line-Item Veto", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 400 && col == 4) {
                JOptionPane.showMessageDialog(null, "Who are the extreme liberals that favor rapid fundamental changes in society?", "Questions: 400",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Radicals", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 500 && col == 4) {
                JOptionPane.showMessageDialog(null, "James Madison, Alexander Hamilton, and John Jay were part of what party?", "Questions: 500",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Federalists Party", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}

            /**
             *   Add Points
             */
            if (type == 200 || type == 400) setImage("e2.png"); // Set to blank image
            else setImage("e1.png"); // Set to blank image
            if (dJ <= 3) type = point; // Use bet instead if Double Jeopary
            if (CW == 1) type *= -1; // If answer is wrong
            nQuestion++; // Increase the number of Question
            Count.check(type); // Add Points
            type = 0; // Rander Button useless
            //x = 0; // Render Button useless
        }
    }    
}

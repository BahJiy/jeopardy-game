import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import java.awt.Font;
import java.awt.Color;

/**
 * Text of all Questions and Answer
 * 
 * @author Huu Vu
 * @version 1.0
 */
public class Text extends Actor
{
    static int nQuestion = 0, // Number of Question Answer (used for Final Jeopary)
    dJchance = 100; // Chances of geting Double Jeopardy 
    int
    type, // Type of Image
    col, // Column question is in
    x, // X
    y; // Y

    Object[] a = {"Valid!", "Invalid."}; // Answer Choices
    public Text(int type) {
        if (type == -6) {
            String text = "Answer: Death in China, Plague in Europe, Reconstruction, Islamic Nations";

            setImage(new GreenfootImage((text.length() + 2) * 10, 16));
            GreenfootImage image = new GreenfootImage(text, 50, Color.RED, new Color(0, 0, 0, 0));
            setImage(image); //Set the Image
        } else if (type == -5) {
            String text = "What are some topic to discuss in the essay for the Black Plague?";

            setImage(new GreenfootImage((text.length() + 2) * 10, 16));
            GreenfootImage image = new GreenfootImage(text, 50, Color.RED, new Color(0, 0, 0, 0));
            setImage(image); //Set the Image
        } else if (type == -1) {setImage("t1.png");}
        else if (type == 0) {setImage("t2.png");}
        else if (type == 1) {setImage("cat.png");}
        else if (type == 2) {setImage("final.png");}
    }

    public Text(int type, int col, int x, int y){
        if (type == 100) {setImage("1.png");}
        else if (type == 200) {setImage("2.png");}
        else if (type == 300) {setImage("3.png");}
        else if (type == 400) {setImage("4.png");}
        else if (type == 500) {setImage("5.png");}        
        this.x = x;
        this.y = y;
        this.type = type;
        this.col = col;
        javax.swing.UIManager.put("OptionPane.messageFont", new FontUIResource(new Font("Times New Roman", Font.PLAIN, 17))); 
    }

    public Text (String text) {
        setImage(new GreenfootImage((text.length() + 2) * 10, 16));
        GreenfootImage image = new GreenfootImage(text, 50, Color.RED, new Color(0, 0, 0, 0));
        setImage(image); //Set the Image
    }

    /**
     * Check Clicks and Display Message
     */
    public void act() 
    {
        // If player click this.Object and this.Object is a question
        if (Greenfoot.mouseClicked(this) && type > 5){
            int 
            CW = -1, // 0 = Correct 1 = Wrong
            dJ = Greenfoot.getRandomNumber(dJchance),
            point = 0, // Point bet
            maxPoint; // Maxium Point Value
            dJchance -= 10; // Make it easier to get DJ
            if(dJchance <= 0)
                dJchance = 1;

            /**
             * Double Jeopary
             */
            if (dJ <= 3) {
                dJchance = 100; // Reset Double Jeopary chances                
                if (Count.score(Count.teamUp) >= 0) // Max Point can bet
                    maxPoint = Count.score(Count.teamUp) + 1000;
                else   maxPoint = 1000;
                Greenfoot.playSound("double.mp3"); // Play Double Jeopary Music
                while (point < 1){
                    String s = JOptionPane.showInputDialog(null, " Input your bet.\n(Have to be between: 1 - " + maxPoint); // Ask for bet amount.
                    try {
                        if (s != null || s.length() >= 1) point = Integer.parseInt(s); // Parse the string into an integer
                    } catch(NumberFormatException e){
                        point = 0;
                    }
                    if (point > maxPoint || point <= 0) {
                        JOptionPane .showMessageDialog  (null, "Invalid Respond!"); // If the answer is 0 or above score + 1000
                        point = 0;
                    }
                }
            }
            // Definition
            if (type == 100 && col == 1) {
                JOptionPane.showMessageDialog(null, "Dhows", "Definition: 100",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "1000-1300.\nShip with Triangular sails that help carry cargo.\nIncrease productivity in African port and allow merchants to trade more with other nations.", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 200 && col == 1) {
                JOptionPane.showMessageDialog(null, "Mandinka", "Definition: 200",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "1000-1300\nMande people that lived in West Africa, west of the Niger River and East of the Sengal River covering about 1,000 miles of land.\nThey establish Mail in 1100 and start mini port city in 1000 that expanded their commercial frontier.","Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 300 && col == 1) {
                JOptionPane.showMessageDialog(null, "Porcelain", "Definition: 300",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Made in China and represent their power and skill.Biggest appearance happened in the 1350-1600 but especially the 1557\nwhen the Portuguese began trading. The demand in trade increase China fame and wealth. It also mixture culture", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 400 && col == 1) {
                JOptionPane.showMessageDialog(null, "Toltecs", "Definition: 400",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "A group of people ranging from migrant groups, refugees and farmers. They start around the 1000.\nThey are known for the mixture of culture in the architecture and art.", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 500 && col == 1) {
                JOptionPane.showMessageDialog(null, "Black Death", "Definition: 500",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Aka the Bubonic Plague. A disease that affected Eurasia, kill almost 2/3 of the population.\nIt started in 1320 in Southwestern China and spread to Europe. It slows down in 1353 and reshape the world's culture\nand create new nations.", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}

            // Define
            if (type == 100 && col == 2) {
                JOptionPane.showMessageDialog(null, "Ottoman Empire", "Define: 100",  JOptionPane.QUESTION_MESSAGE);
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Emerge to exploited the rich resources of the Indian Ocean and Mediterranean Sea basin.\nThey are Turks that expanded due to the demise of the Mongol. They control Anatoli and most of Europe and part of Africa.\nThey took Constantinople for the Islamic people.", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 200 && col == 2) {
                JOptionPane.showMessageDialog(null, "Akbar", "Define: 200",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Aka The Great Mughal. Control India during 1556 to 1605. Increase Mughal trade with the European.\nIn 1578, he allow ambassador and missionary to enter his court thus increasing trade with Europe.", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 300 && col == 2) {
                JOptionPane.showMessageDialog(null, "Renaissance", "Define: 300",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Rebirth of Greek and Roman culture in Europe. Happened in 1430 to 1550. Reshape Europe's culture and bring about Humanism, a focus on human experience rather than religion.", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 400 && col == 2) {
                JOptionPane.showMessageDialog(null, "Vasco da Gama", "Define: 400",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Portuguese mariner that travel to India. \nEstablish a trading route to India in 1498 and establish a stronghold near India.", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 500 && col == 2) {
                JOptionPane.showMessageDialog(null, "Tlaxcalans", "Define: 500",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Moral enemy of the Aztec and helped Cortes defeat the Aztec.\nIn 1519, the Tlaxcalans and Cortes defeat and conquer the Aztec. Defeating the Aztec allow Spanish greater control over South America", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}

            //Define and Defination
            if (type == 100 && col == 3) {
                JOptionPane.showMessageDialog(null, "Potosi", "Define or Definition: 100",  JOptionPane.QUESTION_MESSAGE);
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Present day Bolivia and Zacatecas. A silver mine created in 1545. The access to silver help increase European trade but create inflation in Asia.", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 200 && col == 3) {
                JOptionPane.showMessageDialog(null, "Sugar", "Define or Definition: 200",  JOptionPane.QUESTION_MESSAGE);
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "An affordable sweeter in Europe. Production of sugar began in India around the 1450 to 1600.\nIn the 17 century, sugar became a major import.\nBecause of how cheap sugar is, people can buy more sugar cause more economic summation.", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 300 && col == 3) {
                JOptionPane.showMessageDialog(null, "Mercantilism", "Define or Definition: 300",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "A system in such for one nation to gain wealth, other nation must fail. This start around the 1600 and 1750 in Europe.\nWith this practice, nations increase their power and economy to such that they can wage more wars.", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 400 && col == 3) {
                JOptionPane.showMessageDialog(null, "Tokugawa Shogunate", "Define or Definition: 400",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Control Japan and help unified the nation under one dynasty.\nIn 1603, Ievasu took control and made it a military state. The Shogunate help Japan achieve peace and unification.", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 500 && col == 3) {
                JOptionPane.showMessageDialog(null, "Qing", "Define or Definition: 500",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Happen after the Ming Dynasty collapse due to bad rule. They rule from 1644 to 1911.\nThey impose stricter ruling and help stabilize the economy and expand trade with other nation.", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}

            // Questions
            if (type == 100 && col == 4) {
                JOptionPane.showMessageDialog(null, "Absolutism", "Questions: 100",  JOptionPane.QUESTION_MESSAGE);
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Absolutism is the belief in the absolute power in the ruler or ruling monarch. Between 1500 and 1750 rule in Europe made this claim.\nAfter the chaos and instability of the plague, Absolutism strengthened the State in its mercantilist and competition", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 200 && col == 4) {
                JOptionPane.showMessageDialog(null, "Asante", "Questions: 200",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "It grew in the period of 1701 and expand through 1750. The Asante state help trade slaves and silver to the European state.\nThe trade help increase the number of firearm available to African, increase the number of wars.", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 300 && col == 4) {
                JOptionPane.showMessageDialog(null, "Captain James Cook", "Questions: 300",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "A European sailor tasked with scientific experiment and is consider one of the most famous figure in European cultural history. In 1780, Cook sailed with Linnaeus to conduct science experiment for the Royal Society.\nBecause of this adventure, European scientists now have the knowledge of multiple plants and animals that people never saw before. He brought Oceanic culture to Europe such as different people like Omai.", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 400 && col == 4) {
                JOptionPane.showMessageDialog(null, "Creole", "Questions: 400",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "European descent who were born in America mark a new power class in America. They came into power between 1500 and 1780. They hate the peninsular. Because of their mix, they help combine culture and race together.", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}
            else if (type == 500 && col == 4) {
                JOptionPane.showMessageDialog(null, "Nothing Here", "Questions: 500",  JOptionPane.QUESTION_MESSAGE); 
                while (CW == -1)  {CW = JOptionPane.showOptionDialog(null, "Nothing Here", "Answer", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); }}

            /**
             *   Add Points
             */
            if (type == 200 || type == 400) setImage("e2.png"); // Set to blank image
            else setImage("e1.png"); // Set to blank image
            if (dJ <= 3) type = point; // Use bet instead if Double Jeopary
            if (CW == 1) type *= -1; // If answer is wrong
            nQuestion++; // Increase the number of Question
            Count.check(type); // Add Points
            type = 0; // Rander Button useless
            //x = 0; // Render Button useless
        }
    }    
}

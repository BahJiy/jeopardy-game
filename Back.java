import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import javax.swing.*; // JOptionPane
import java.util.Collections; // Shuffle List
import java.util.*; // List
import java.io.IOException;
/**
 * Background Picture and holds the Text.class as a List
 * 
 * @author Huu Vu
 * @version 1.0
 */
public class Back extends World
{
    List<Text> tList = new ArrayList<Text>(); // Holds the Text.class
    GreenfootSound s1 = new GreenfootSound("s1.mp3"); // Sound Effect
    boolean StartUp = true; // Start up
    /**
     * Background Picture
     */
    public Back()
    {    
        super(799, 580, 1);
    }

    public void act() {
        if (StartUp) {       
            // Add Text.class to List
            // Nature        
            tList.add(new Text(100, 1, 100, 206));
            tList.add(new Text(200, 1, 100, 291));
            tList.add(new Text(300, 1, 100, 376));         
            tList.add(new Text(400, 1, 100, 460));
            tList.add(new Text(500, 1, 100, 543));
            // Action
            tList.add(new Text(100, 2, 300, 206));
            tList.add(new Text(200, 2, 300, 291));
            tList.add(new Text(300, 2, 300, 376));
            tList.add(new Text(400, 2, 300, 460));
            tList.add(new Text(500, 2, 300, 543));
            // Object
            tList.add(new Text(100, 3, 500, 206));
            tList.add(new Text(200, 3, 500, 291));
            tList.add(new Text(300, 3, 500, 376));
            tList.add(new Text(400, 3, 500, 460));
            tList.add(new Text(500, 3, 500, 543));
            // People
            tList.add(new Text(100, 4, 700, 206));
            tList.add(new Text(200, 4, 700, 291));        
            tList.add(new Text(300, 4, 700, 376));
            tList.add(new Text(400, 4, 700, 460));        
            //tList.add(new Text(500, 4, 700, 543));

            Collections.shuffle(tList);
            // Subject
            addObject(new Text(1), 399, 122);  
            addObject(new Text(-1), 200, 38); 
            addObject(new Text(0), 600, 38);
            
            Greenfoot.playSound("s1.mp3");   
            Greenfoot.delay(70);
            for (int x = tList.size() - 1; x >= 0; x--){
                addObject(tList.get(x), tList.get(x).x, tList.get(x).y); Greenfoot.delay(8);
            }

            s1.stop();
            int x = Greenfoot.getRandomNumber(4);
            if (x == 0) {JOptionPane.showMessageDialog(null, "Team 1 Goes First!");
            } else if (x == 1) {JOptionPane.showMessageDialog(null, "Team 2 Goes First!");
            } else if (x == 2) {JOptionPane.showMessageDialog(null, "Team 3 Goes First!"); 
            } else if (x == 3) {JOptionPane.showMessageDialog(null, "Team 4 Goes First!");
            }        

            addObject(new Count(x + 1), 400, 40);
            StartUp = false;
        }
        else if (Text.nQuestion == 19) {            
            End e = new End();   
            Object[] a = {"Valid!", "Invalid."};
            int wage = 0;

            Greenfoot.delay(10);
            Greenfoot.playSound("final.mp3");
            Text.nQuestion += 1;
            JOptionPane.showMessageDialog(null, "Time For Final Jeopardy!");

            String s = "";
            int maxPoint = 0;
            for (int x = 1; x <= 4; x++){
                wage = 0;
                if (Count.score(x) >= 0) // Max Point can bet
                    maxPoint = Count.score(x) + 2500;
                else  
                    maxPoint = 2500;
                while(wage <= 0){
                    s = JOptionPane.showInputDialog(null,"Input your wager! Max is: " + maxPoint + "\nTeam " + x + ":", "0");
                    try{
                        if (s != null && s != "") wage = Integer.parseInt(s);
                    } catch (NumberFormatException expection) {wage = 0;};
                    if (wage <= 0 || wage > maxPoint) {
                        JOptionPane.showMessageDialog(null, "Invalid Respond!");  
                        wage = 0;
                    }
                    else {
                        if (x == 1) 
                            e.w1 = wage; 
                        if (x == 2) 
                            e.w2 = wage; 
                        if (x == 3) 
                            e.w3 = wage; 
                        if (x == 4) 
                            e.w4 = wage; 
                    }
                }
            } wage = -1;

            removeObjects(getObjects(null));
            addObject(new Text(2), 402,287);
            JOptionPane.showMessageDialog(null, "The Question is:");
            addObject(new Text(-5), 404, 298);
            Greenfoot.playSound("think.mp3");            
            Greenfoot.delay(1900);
            Greenfoot.playSound("time's up.mp3");
            JOptionPane.showMessageDialog(null, "TIME! What are the answers?");
            addObject(new Text(-6), 405, 390);
            Greenfoot.delay(20);
            while (wage == -1)  {wage = JOptionPane.showOptionDialog(null, "Player 1 Answer Valid?", "Answer", 
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); } e.finalJ(1, wage); wage = -1;
            while (wage == -1)  {wage = JOptionPane.showOptionDialog(null, "Player 2 Answer Valid?", "Answer", 
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); } e.finalJ(2, wage); wage = -1;
            while (wage == -1)  {wage = JOptionPane.showOptionDialog(null, "Player 3 Answer Valid?", "Answer", 
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); } e.finalJ(3, wage); wage = -1;
            while (wage == -1)  {wage = JOptionPane.showOptionDialog(null, "Player 4 Answer Valid?", "Answer", 
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, a, a[0]); } e.finalJ(4, wage);
            addObject(new Count(0), 403, 56);
            addObject(e, 403, 96);

            List<List<Integer>> score = new ArrayList<List<Integer>>() {{
                        add(Arrays.asList(Count.team1, 1));
                        add(Arrays.asList(Count.team2, 2));
                        add(Arrays.asList(Count.team3, 3));
                        add(Arrays.asList(Count.team4, 4));
                    }};

            int current = 1;
            for (int x = 1; x <= 3; x++){
                if (score.get(0).get(0) >  score.get(current).get(0))
                    score.remove(current);
                else if (score.get(0).get(0) < score.get(current).get(0)) {
                    while (current != 0) {
                        current--;
                        score.remove(current);                        
                    }
                    current = 1;
                }
                else if (score.get(0).get(0) == score.get(current).get(0))
                    current++;                             
            }

            String text = "Teams ";
            if (score.size() > 1){   
                for (int x = 0; x <= score.size() -1; x++){
                    if (x == score.size() - 1)
                        text += "and " + score.get(x).get(1);
                    else
                        text += score.get(x).get(1) + ", ";
                }
                text += " Tied!";
                
                JOptionPane.showMessageDialog(null, text);
            } else {
                text += score.get(0).get(1) + " Won!";
                JOptionPane.showMessageDialog(null, text);
                addObject(new Text(text), 400, 175);
            }
        }
    }
}

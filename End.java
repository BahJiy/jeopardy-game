import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import javax.swing.*;
import java.awt.Color;
/**
 * Last Question
 * 
 * @author Huu Vu
 * @version 1.0
 */
public class End extends Actor
{
    String text = "";
    int w1, w2, w3, w4; // Player's Wages

    /**
     * Calculate Final Points
     */
    public void finalJ(int x, int y) {
        if (x == 1) {
            if (y == 1) Count.team1 -= w1;
            else if (y == 0) Count.team1 += w1;
        } else if (x == 2) {
            if (y == 1) Count.team2 -= w2;
            else if (y == 0) Count.team2 += w2;
        } else if (x == 3) {
            if (y == 1) Count.team3 -= w3;
            else if (y == 0) Count.team3 += w3;
        } else if (x == 4) {
            if (y == 1) Count.team4 -= w4;
            else if (y == 0) Count.team4 += w4;
        }
    }

    /**
     * Act - do whatever the End wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        text = "\t\tWages:\nTeam 1: " + w1 + "    Team 2: " + w2 + "    Team 3: " + w3 + "    Team 4: " + w4;
        UpdateImage();
    }    

    /**
     * Make the image/Update Image
     */
    private void UpdateImage() {
        setImage(new GreenfootImage((text.length() + 2) * 10, 16));
        GreenfootImage image = new GreenfootImage(text, 30, Color.RED, new Color(0, 0, 0, 0));
        setImage(image); //Set the Image     
    }
}
